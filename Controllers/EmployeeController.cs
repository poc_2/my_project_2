﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using System.Data.SqlClient;
using TestAPICore.Models;
using TestAPICore.Repository;

namespace TestAPICore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private IConfiguration Configuration;
        public List<Employee> employees = new List<Employee>();
        private IEmployee emp;

        public EmployeeController(IEmployee _emp)
        {
            emp = _emp;
        }

        [HttpGet(Name = "GetEmployees")]
        public List<Employee> Get()
        {
            return emp.GetEmployee();
        }

        [HttpPost(Name = "InsertEmployees")]
        public string Insert(string strName)
        {
            return emp.InsertEmployee(strName);
        }

        [HttpPut(Name = "UpdateEmployees")]
        public bool Update(Employee emp)
        {
            int result = 0;
            using (SqlConnection conn = new SqlConnection(Configuration.GetConnectionString("MyConn")))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("proc_UpdateEmployee", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id", SqlDbType.Int).Value = emp.Id;
                cmd.Parameters.AddWithValue("@Name", SqlDbType.VarChar).Value = emp.Name;
                result = cmd.ExecuteNonQuery();
                conn.Close();
            }
            return result == 0 ? false : true;
            //var empData = employees.Where(r => r.Id == emp.Id).FirstOrDefault();
            //if (empData != null)
            //{
            //    empData.Name = emp.Name;
            //}
            //return true;
        }

        [HttpDelete(Name = "DeleteEmployees")]
        public bool Delete(int Id)
        {
            int result = 0;
            using (SqlConnection conn = new SqlConnection(Configuration.GetConnectionString("MyConn")))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("proc_DeleteEmployee", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id", SqlDbType.Int).Value = Id;
                result = cmd.ExecuteNonQuery();
                conn.Close();
            }
            return result == 0 ? false : true;
        }
    }
}
