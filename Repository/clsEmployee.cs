﻿using Npgsql;
using System.Data;
using TestAPICore.Models;

namespace TestAPICore.Repository
{
    public class clsEmployee : IEmployee
    {
        IConfiguration Configuration;
        public clsEmployee(IConfiguration _Configuration)
        {
            Configuration = _Configuration;
        }

        public List<Employee> GetEmployee()
        {
            List<Employee> empList = new List<Employee>();
            using (NpgsqlConnection conn = new NpgsqlConnection(Configuration.GetConnectionString("MyConn")))
            {
                conn.Open();
                NpgsqlCommand cmd = new NpgsqlCommand("fnGetEmployees", conn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                NpgsqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    empList.Add(new Employee
                    {
                        Id = Convert.ToInt32(dr["Id"]),
                        Name = Convert.ToString(dr["Name"]),
                        IsDeleted = Convert.ToBoolean(dr["IsDeleted"])
                    });
                }
                conn.Close();
            }
            return empList;
        }

        public string InsertEmployee(string strName)
        {
            string result = string.Empty;
            using (NpgsqlConnection conn = new NpgsqlConnection(Configuration.GetConnectionString("MyConn")))
            {
                conn.Open();
                NpgsqlCommand cmd = new NpgsqlCommand("call proc_InsertEmployee(:Name_In, '');", conn);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("Name_In", DbType.String).Value = strName;
                NpgsqlParameter outMsg = new NpgsqlParameter("msg", DbType.String)
                {
                    Direction = ParameterDirection.Output
                };
                cmd.Parameters.Add(outMsg);
                int i = cmd.ExecuteNonQuery();
                result = Convert.ToString(outMsg.Value);
                conn.Close();
            }
            return result;
        }
    }
}
