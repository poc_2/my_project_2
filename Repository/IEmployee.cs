﻿using TestAPICore.Models;

namespace TestAPICore.Repository
{
    public interface IEmployee
    {
        public List<Employee> GetEmployee();
        public string InsertEmployee(string strName);
    }
}
